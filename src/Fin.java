import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import java.net.URL;
import java.awt.Dimension;


public class Fin  extends JFrame implements ActionListener  {	
	

	private JButton jugarDeNuevo;
	private JPanel panel;
	private FinImagen imagen;
  
	public Fin(int resultado){
		
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Fin");
		
		imagen = new FinImagen();
		jugarDeNuevo= new JButton("Jugar de nuevo");
		panel = new JPanel();
		
		jugarDeNuevo.addActionListener(this);
		
		jugarDeNuevo.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		jugarDeNuevo.setBackground(Color.gray);
		jugarDeNuevo.setForeground(Color.white);
				
		imagen.victoriaOFracaso(resultado);
		imagen.repaint();
		
		this.setLayout(new BorderLayout());
		this.add(jugarDeNuevo, BorderLayout.SOUTH);
		this.add(imagen, BorderLayout.CENTER);

		
		this.setVisible(true);
		this.setSize(1200,1200);
		
	}
	


	@Override
	public void actionPerformed(ActionEvent ae) {
		
		if(ae.getSource().equals(jugarDeNuevo)){
			Inicio aplicacion = new Inicio();
			aplicacion.setVisible(true);
			dispose();
		}	
	}
	
	
}