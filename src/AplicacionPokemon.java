import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;

public class AplicacionPokemon  extends JFrame implements ActionListener {
	private JButton pausa, continuar, eligeAgua, eligeAire, eligeFuego, eligeTierra, atacar, jugarDeNuevo;
	private JTextArea area;
	private JPanel panelPrincipal, panelBotones;
	private Juego juego;
	private Timer tiempo;
	private Inicio inicio;
	private int opcion;
	

	
	
	public AplicacionPokemon(int op){
		opcion=op;
		//Conecta con la aplicacion
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Batalla Pokem�n");
		
		pausa = new JButton("Pausa");
		continuar = new JButton("Continuar");
		eligeAgua = new JButton("Agua"); eligeAire = new JButton("Aire"); eligeFuego = new JButton("Fuego"); eligeTierra = new JButton("Tierra");
		atacar = new JButton("Ataca");
		jugarDeNuevo = new JButton("Juegar de nuevo");
		area = new JTextArea();
		panelPrincipal= new JPanel();
		panelBotones = new JPanel();
		juego = new Juego();
		tiempo = new Timer(1900, this);

	
		pausa.addActionListener(this);
		continuar.addActionListener(this);
		eligeAgua.addActionListener(this); eligeAire.addActionListener(this); eligeFuego.addActionListener(this); eligeTierra.addActionListener(this);
		atacar.addActionListener(this);
		jugarDeNuevo.addActionListener(this);
		
		atacar.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		pausa.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		continuar.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		area.setFont(new Font("Helveltica", Font.PLAIN, 30));
		
		atacar.setBackground(Color.gray);
		pausa.setBackground(Color.gray);
		continuar.setBackground(Color.gray);
		area.setBackground(new Color(191,237,255));
		
		atacar.setForeground(Color.red);
		continuar.setForeground(Color.green);
		pausa.setForeground(Color.blue);
		
		panelBotones.setLayout(new GridLayout(1,3));
		panelBotones.add(atacar);
		panelBotones.add(pausa);
		panelBotones.add(continuar);
		
		
		
		panelPrincipal.setLayout(new GridLayout(2,1));
		panelPrincipal.add(panelBotones);
		panelPrincipal.add(area);
		
		this.setLayout(new BorderLayout());
		this.add(panelPrincipal, BorderLayout.SOUTH);
		this.add(juego, BorderLayout.CENTER);

		this.setVisible(true);
		this.setSize(1200,1200);
		
		juego.escogeAmigo(opcion);
		juego.escogeEnemigo(opcion);
		tiempo.start();
		
	
	

	
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		String texto=""; 
		
		if (ae.getSource().equals(continuar)) {
			tiempo.start();
		}
		if (ae.getSource().equals(pausa)) {
			tiempo.stop();
		}
		if(ae.getSource().equals(atacar)){
			texto="";
			
			juego.atacaAlEnemigo();
			juego.setBanderaAtaque(true);
			juego.repaint();

			
			texto+=juego.escribeAlEnemigo();
			area.setText(texto);
			juego.atacaAlAmigo();
			juego.repaint();
			
			texto+="\n"+juego.escribeAlAmigo();
			area.setText(texto);
			//ganaste
			if(juego.fin()==1){
				Fin aplicacion = new Fin(1);
				aplicacion.setVisible(true);
				dispose();
			}
			
			//perdiste
			if(juego.fin()==2){
				Fin aplicacion = new Fin(2);
				aplicacion.setVisible(true);
				dispose();
			}
		}
		
		if(ae.getSource().equals(tiempo)){
			
			juego.muevePersonajes();
			juego.repaint();
			juego.setBanderaAtaque(false);

			
			
			
		}
		
	}
}
