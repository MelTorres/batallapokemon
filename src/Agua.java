import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Agua extends Pokemon {
    

	//amigo
	public Agua(){
		super("Squirtle", 200, 805);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Agua_Amigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("squirtleAtaqueAmigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Squir.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));

	}
	//enemigo
	public Agua(String vacio){
		super("Squirtle", 675, 225);		
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Agua_Enemigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("squirtleAtaqueEnemigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Squir.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));

	}
	

	public int effectiveness (Pokemon obj) {
		if(obj instanceof Aire){ 
			return 10;
		}
		else if(obj instanceof Tierra){
			return 20;
		}
		else if(obj instanceof Fuego){
			return 40;
		}
		else return 0;
	}
	
	public void dibujate(Graphics g) {
		g.drawImage(imagen.getImage(), x, y, null);
	}
	
	public void animacionAtaque(Graphics g, boolean amigo){
		if(amigo){
			g.drawImage(imagenAtaque.getImage(), x+250, y-75 , null);
		}
		else{
			 g.drawImage(imagenAtaque.getImage(), x-250, y+109, null);

		}
	}
}
