import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;

public class ImagenesHp {
	
	ImageIcon hp100, hp90, hp80, hp70, hp60, hp50, hp40, hp30, hp20, hp10, hp0;
	LinkedList<ImageIcon> hp=new LinkedList<ImageIcon>();
	public ImagenesHp(){
		hp=null;
		hp100 = new ImageIcon(new ImageIcon(getClass().getResource("100.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp90 = new ImageIcon(new ImageIcon(getClass().getResource("90.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp80 = new ImageIcon(new ImageIcon(getClass().getResource("800.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp70 = new ImageIcon(new ImageIcon(getClass().getResource("70.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp60 = new ImageIcon(new ImageIcon(getClass().getResource("60.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp50 = new ImageIcon(new ImageIcon(getClass().getResource("50.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp40 = new ImageIcon(new ImageIcon(getClass().getResource("40.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp30 = new ImageIcon(new ImageIcon(getClass().getResource("30.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp20 = new ImageIcon(new ImageIcon(getClass().getResource("20.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp10 = new ImageIcon(new ImageIcon(getClass().getResource("10.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));
		hp0 = new ImageIcon(new ImageIcon(getClass().getResource("0.jpg")).getImage().getScaledInstance(100, 12,  java.awt.Image.SCALE_SMOOTH));

	}
	
	public void agregaImagen(){
		hp.add(hp0);
		hp.add(hp10);
		hp.add(hp20);
		hp.add(hp30);
		hp.add(hp40);
		hp.add(hp50);
		hp.add(hp60);
		hp.add(hp70);
		hp.add(hp80);
		hp.add(hp90);
		hp.add(hp100);
	}
	//recibe el hp y regresa su respectiva imagen
	public ImageIcon getHpImagen(int inthp){
		int index=inthp/10;
		return hp.get(index);
	}


}
