

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import java.net.URL;
import java.awt.Dimension;


public class HasElegido  extends JFrame implements ActionListener  {	
	

	private JButton comenzar, regresar;
	private JPanel panel;
	private HasElegidoImagen imagen;
	private int opcion;
  

	public HasElegido(int op){
		opcion = op;
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("HasElegido");
		
		imagen = new HasElegidoImagen();
		comenzar= new JButton("Comenzar");
		regresar = new JButton("Regresar");
		panel = new JPanel();
		
		comenzar.addActionListener(this);
		regresar.addActionListener(this);
		
		comenzar.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		regresar.setFont(new Font("Cooper Black", Font.PLAIN, 55));
		
		comenzar.setBackground(Color.gray);
		regresar.setBackground(Color.gray);

		comenzar.setForeground(Color.white);
		regresar.setForeground(Color.white);
				
		panel.setLayout(new GridLayout(1, 2));
		panel.add(regresar);
		panel.add(comenzar);
		imagen.escogeAmigo(opcion);

		imagen.repaint();
		
		this.setLayout(new BorderLayout());
		this.add(panel, BorderLayout.SOUTH);
		this.add(imagen, BorderLayout.CENTER);

		
		this.setVisible(true);
		this.setSize(1200,1200);
		
	}
	



	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource().equals(comenzar)){
		
		AplicacionPokemon aplicacion = new AplicacionPokemon(opcion);
		aplicacion.setVisible(true);
		dispose();
		}
		else if(ae.getSource().equals(regresar)){
			Inicio aplicacion = new Inicio();
			aplicacion.setVisible(true);
			dispose();
			
		}
		
		
	}
	
	
}
