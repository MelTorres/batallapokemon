import java.awt.Graphics;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.ImageIcon;

public abstract class Pokemon {
	
	protected int hp;
	protected int level;
	protected String nombre;
	protected int x;
	protected int y;
	protected boolean acerto;
	protected ImageIcon imagen, imagenAtaque;
	protected ImageIcon imagenNombre,imagenesHp1,imagenesHp2,imagenesHp3,imagenesHp4,imagenesHp5,imagenesHp6,imagenesHp7,imagenesHp8,imagenesHp9,imagenesHp10,imagenesHp0;
	protected LinkedList <ImageIcon> imagenesHp;
	protected int k1, k2;

	public Pokemon(String n, int posx, int posy){
		hp=100;
		level=5;
		nombre=n;
		x=posx;
		y=posy;	
		acerto=true;
		imagen = null;
		k1=10;
		k2=30;
		imagenAtaque= null;
		
		imagenesHp0 = new ImageIcon(new ImageIcon(getClass().getResource("0.png")).getImage().getScaledInstance(350, 40,java.awt.Image.SCALE_SMOOTH));
		imagenesHp1 = new ImageIcon(new ImageIcon(getClass().getResource("10.png")).getImage().getScaledInstance(350, 40, java.awt.Image.SCALE_SMOOTH));
		imagenesHp2 = new ImageIcon(new ImageIcon(getClass().getResource("20.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp3 = new ImageIcon(new ImageIcon(getClass().getResource("30.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp4 = new ImageIcon(new ImageIcon(getClass().getResource("40.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp5 = new ImageIcon(new ImageIcon(getClass().getResource("50.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp6 = new ImageIcon(new ImageIcon(getClass().getResource("60.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp7 = new ImageIcon(new ImageIcon(getClass().getResource("70.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp8 = new ImageIcon(new ImageIcon(getClass().getResource("80.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp9 = new ImageIcon(new ImageIcon(getClass().getResource("90.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		imagenesHp10 = new ImageIcon(new ImageIcon(getClass().getResource("100.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));

		
		imagenesHp = new LinkedList <ImageIcon>();
		imagenesHp.add(imagenesHp0);
		imagenesHp.add(imagenesHp1);
		imagenesHp.add(imagenesHp2);
		imagenesHp.add(imagenesHp3);
		imagenesHp.add(imagenesHp4);
		imagenesHp.add(imagenesHp5);
		imagenesHp.add(imagenesHp6);
		imagenesHp.add(imagenesHp7);
		imagenesHp.add(imagenesHp8);
		imagenesHp.add(imagenesHp9);
		imagenesHp.add(imagenesHp10);
		
		imagenNombre=null;
	
	}

	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	
	public boolean isAcerto() {
		return acerto;
	}

	public void setAcerto(boolean acerto) {
		this.acerto = acerto;
	}

	public ImageIcon getImagen() {
		return imagen;
	}

	public void setImagen(ImageIcon imagen) {
		this.imagen = imagen;
	}

	public int muerte(Pokemon enemigo){
		
		if(enemigo.getHp()<=0){
			return 1;
		}
		else if(hp<=0){
			return 2;
		}
		else return 0;
		
	}
	
	public boolean acertar(){
		Random rand= new Random();
		int aleatorio = rand.nextInt(100);
		if(aleatorio>=75){
			return false;
		}
		else{
			return true;
		}
		
	}


	public void ataca(Pokemon obj) {
		acerto=this.acertar();
		if(acerto){
			obj.setHp(obj.getHp()-this.effectiveness(obj));
		}
	
	}
	
	public String escribeAtaque(Pokemon obj){
		if(acerto){
			return nombre + " ha atacado a "+ obj.getNombre()+" y le ha hecho "+this.effectiveness(obj)+" de da�o!";
		}
		
		else{
			return nombre + " ha fallado el ataque! ";
		}
		
		
	}
	public abstract int effectiveness (Pokemon obj);
	public abstract void dibujate(Graphics g);
	public abstract void animacionAtaque(Graphics g, boolean amigo);
	
	public void muevete (){
		y+=k1;
		k1*=-1;

	}

	public void dibujaHp(Graphics g, boolean amigo){
		if(amigo){
		g.drawImage(imagenNombre.getImage(), x+550, 850, null);
		g.drawImage(imagenesHp.get(this.hp/10).getImage(), x+550, 900, null);
		}
		else{
			g.drawImage(imagenNombre.getImage(), x-500, 250, null);
			g.drawImage(imagenesHp.get(this.hp/10).getImage(), x-500, 300, null);

			
		}

		
	}
	
	
	public ImageIcon getImagenAtaque() {
		return imagenAtaque;
	}

	public void setImagenAtaque(ImageIcon imagenAtaque) {
		this.imagenAtaque = imagenAtaque;
	}




	

}
