import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Tierra extends Pokemon {

	
	public Tierra(){
		super("Rattata", 200, 805);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Tierra_amigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("ratattaAtaqueAmigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Ratata.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));

	}
	public Tierra(String vacio){
		super("Rattata",675, 225);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Tierra_enemigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("ratattaAtaqueAmigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Ratata.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
	}

	public int effectiveness(Pokemon obj) {
		if(obj instanceof Aire){
			return 40;
		}
		else if(obj instanceof Fuego){
			return 10;
		}
		else if(obj instanceof Agua){
			return 20;
		}
		else return 0;

	}
	public void dibujate(Graphics g) {
		g.drawImage(imagen.getImage(), x, y, null);
	}
	public void animacionAtaque(Graphics g, boolean amigo) {
		if(amigo){
		 g.drawImage(imagenAtaque.getImage(), x+(275), y-75, null);
		}
		else{
		 g.drawImage(imagenAtaque.getImage(), x+(150), y-75, null);

		}
	}

}
