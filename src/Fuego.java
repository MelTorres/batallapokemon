import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Fuego extends Pokemon {

	public Fuego(){
		super("Charmander", 200, 805);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Fuego_Amigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("charmanderAtaqueAmigo.png")).getImage().getScaledInstance(270, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Char.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		
	}
	public Fuego( String vacio){
		super("Charmander", 675, 225);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Fuego_enemigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("charmanderAtaqueEnemigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Char.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
	}

	public int effectiveness(Pokemon obj) {
		if(obj instanceof Aire){
			return 20;
		}
		else if(obj instanceof Tierra){
			return 40;
		}
		else if(obj instanceof Agua){
			return 10;
		}
		else return 0;

	}
	public void dibujate(Graphics g) {
		g.drawImage(imagen.getImage(), x, y, null);
	}

	public void animacionAtaque(Graphics g, boolean amigo) {
		if(amigo){
			 g.drawImage(imagenAtaque.getImage(), x+(275), y-75, null);
			}
			else{
			 g.drawImage(imagenAtaque.getImage(), x-(175), y+135, null);

			}		
	}
	
	
}
