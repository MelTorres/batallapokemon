import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import java.net.URL;
import java.awt.Dimension;


public class Inicio extends JFrame implements ActionListener {
	
	private JButton eligeAgua, eligeAire, eligeFuego, eligeTierra;
	private JPanel panel;
	private int opcion;
    private ImageIcon imagenAgua, imagenAire, imagenFuego, imagenTierra;
    private ImageIcon logoPokemon;
    private JLabel label;
    

	public Inicio (){
		//Conecta con la aplicacion
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Inicio");
		
		logoPokemon = new ImageIcon(new ImageIcon(getClass().getResource("logoPokemon.png")).getImage().getScaledInstance(567, 227,  java.awt.Image.SCALE_SMOOTH));
		imagenAgua = new ImageIcon(new ImageIcon(getClass().getResource("squirtleBanner.jpg")).getImage().getScaledInstance(1200, 225,  java.awt.Image.SCALE_SMOOTH));
		imagenAire = new ImageIcon(new ImageIcon(getClass().getResource("pidgeyBanner.jpg")).getImage().getScaledInstance(1200, 225,  java.awt.Image.SCALE_SMOOTH));
		imagenFuego = new ImageIcon(new ImageIcon(getClass().getResource("charmanderBanner.jpg")).getImage().getScaledInstance(1200, 225,  java.awt.Image.SCALE_SMOOTH));
		imagenTierra = new ImageIcon(new ImageIcon(getClass().getResource("rattataBanner.jpg")).getImage().getScaledInstance(1200, 225,  java.awt.Image.SCALE_SMOOTH));
		label = new JLabel(logoPokemon);
		opcion = 0;
		
		eligeAgua = new JButton("", imagenAgua);
		eligeAire = new JButton("", imagenAire);
		eligeFuego = new JButton("", imagenFuego);
		eligeTierra = new JButton("", imagenTierra);
		panel = new JPanel();
		
		
		eligeAgua.addActionListener(this);
		eligeAire.addActionListener(this);
		eligeFuego.addActionListener(this);
		eligeTierra.addActionListener(this);
		
		eligeAgua.setFont(new Font("Helveltica", Font.PLAIN, 30));
		eligeAire.setFont(new Font("Helveltica", Font.PLAIN, 30));
		eligeFuego.setFont(new Font("Helveltica", Font.PLAIN, 30));
		eligeTierra.setFont(new Font("Helveltica", Font.PLAIN, 30));

		panel.setLayout(new GridLayout(5,1));
		panel.add(label);
		panel.add(eligeAgua);
		panel.add(eligeAire);
		panel.add(eligeFuego);
		panel.add(eligeTierra);
		
		this.setLayout(new BorderLayout());
		this.add(panel, BorderLayout.CENTER);
		
		this.setVisible(true);
		this.setSize(1200,1200);
		
	}
	
	public int getOpcion() {
		return opcion;
	}

	public void setOpcion(int opcion) {
		this.opcion = opcion;
	}

	public static void main(String[] args) {
		new Inicio();	
		Musicaa play = new Musicaa();
		play.reproducir("Opening_Pokemon.mp3.mp3");
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		
		if(ae.getSource().equals(eligeAgua)){
			this.opcion=1;
		}
		else if(ae.getSource().equals(eligeAire)){
			this.opcion=2;
		}
		else if(ae.getSource().equals(eligeTierra)){
			this.opcion=4;
		}
		else if(ae.getSource().equals(eligeFuego)){
			this.opcion=3;
		}
		
		//AplicacionPokemon aplicacion = new AplicacionPokemon(opcion);
		HasElegido aplicacion = new HasElegido(opcion);
		aplicacion.setVisible(true);
		dispose();
		
	}
	
	
}
