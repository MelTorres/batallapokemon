import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Juego extends JPanel{
	private Pokemon amigo, enemigo;
	private ImageIcon fondo, imagenAmigo, imagenEnemigo, imagenHpAmigo, imagenHpEnemigo;
	private boolean banderaAtaque;
	
	
	public Juego (){
		banderaAtaque=false;
		amigo=null;
		enemigo=null;
		fondo = new ImageIcon(new ImageIcon(getClass().getResource("fondo.png")).getImage().getScaledInstance(1200, 1200,  java.awt.Image.SCALE_SMOOTH));
		imagenAmigo = null;
		imagenEnemigo = null;

		
		this.setVisible(true); 
		this.setSize(1200, 1200);

	}
	public boolean isBanderaAtaque() {
		return banderaAtaque;
	}
	public void setBanderaAtaque(boolean banderaAtaque) {
		this.banderaAtaque = banderaAtaque;
	}
	public void paint(Graphics g) {
		
		g.drawImage(fondo.getImage(), 0, 0, null);
		amigo.dibujate(g);
		enemigo.dibujate(g);
		amigo.dibujaHp(g, true);
		enemigo.dibujaHp(g, false);
		
		if(banderaAtaque){
			amigo.animacionAtaque(g, true);
			enemigo.animacionAtaque(g, false);
		}
	}
	
	
	
	public ImageIcon getImagenAtaqueAlEnemigo(){
		return amigo.getImagenAtaque();
	}
	
	public ImageIcon getImagenAtaqueAlAmigo(){
		return enemigo.getImagenAtaque();
	}
	
	

	public void escogeAmigo (int opcion){
		switch(opcion){
		case 1:			//Agua
			amigo = new Agua();
			break;
		case 2:			//Aire	
			amigo = new Aire();
			break;	
		case 3:			//Fuego
			amigo = new Fuego();
			break;
		case 4:			//Tierra
			amigo = new Tierra();
			break;
		default:
			break;
		}
		
	}	
	public void escogeEnemigo (int opcion){
		String vacio="";
		Random rand= new Random();
		int opcionEnemigo;
		do{
		opcionEnemigo = rand.nextInt(4)+1;
		}while (opcionEnemigo==opcion);
		
		switch(opcionEnemigo){
		case 1:			//Agua
			enemigo = new Agua(vacio);
			break;
		case 2:			//Aire	
			enemigo = new Aire(vacio);
			break;	
		case 3:			//Fuego
			enemigo = new Fuego(vacio);
			break;
		case 4:			//Tierra
			enemigo = new Tierra(vacio);
			break;
		default:
			break;
		}
		
	}
	
	
	public void atacaAlEnemigo(){
		amigo.ataca(enemigo);
	}
	public void atacaAlAmigo(){
		enemigo.ataca(amigo);
	}
	public String escribeAlAmigo(){
		return enemigo.escribeAtaque(amigo);
	}
	public String escribeAlEnemigo(){
		return amigo.escribeAtaque(enemigo);
	}
	public int fin (){
		return amigo.muerte(enemigo);
	}
	
	public void muevePersonajes(){
		amigo.muevete();
		enemigo.muevete();
	}
	
	
}
