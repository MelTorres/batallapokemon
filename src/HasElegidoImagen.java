import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class HasElegidoImagen extends JPanel{
	private Pokemon amigo, enemigo;
    private ImageIcon imagen;
	private int opcion;

	
	
	
	public HasElegidoImagen (){
		imagen = null;
		
		this.setVisible(true); // panel, que se pueda ver
		this.setSize(1200, 1200);

	}
	public void paint(Graphics g) {

		
		g.drawImage(imagen.getImage(), 0, 0, null);
		
		
	}
	

	public void escogeAmigo (int opcion){
		switch(opcion){
		case 1:			//Agua
			imagen = new ImageIcon(new ImageIcon(getClass().getResource("squirtleJugemos.jpg")).getImage().getScaledInstance(1200, 1200,  java.awt.Image.SCALE_SMOOTH));
			break;
		case 2:			//Aire	
			imagen = new ImageIcon(new ImageIcon(getClass().getResource("pidgeyJugemos.png")).getImage().getScaledInstance(1200, 1200,  java.awt.Image.SCALE_SMOOTH));
		break;	
		
		case 3:			//Fuego
			imagen = new ImageIcon(new ImageIcon(getClass().getResource("charmanderJugemos.jpg")).getImage().getScaledInstance(1200, 1200,  java.awt.Image.SCALE_SMOOTH));
			break;
		case 4:			//Tierra
			imagen = new ImageIcon(new ImageIcon(getClass().getResource("rattataJugemos.png")).getImage().getScaledInstance(1200, 1200,  java.awt.Image.SCALE_SMOOTH));
			break;
		default:
			break;
		}}
		
	}	
	
