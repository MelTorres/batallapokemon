import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Aire extends Pokemon {

	
	public Aire(){
		super("Pidgey", 200, 805);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Aire_Amigo.png")).getImage().getScaledInstance(285, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("pidgeyAtaque.jpg")).getImage().getScaledInstance(250, 240,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Pid.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));
		
	}	
	//enemigo
	public Aire(String vacio){
		super("Pidgey", 675, 225);
		imagen = new ImageIcon(new ImageIcon(getClass().getResource("Aire_Enemigo.png")).getImage().getScaledInstance(280, 190,  java.awt.Image.SCALE_SMOOTH));
		imagenAtaque = new ImageIcon(new ImageIcon(getClass().getResource("pidgeyAtaque.jpg")).getImage().getScaledInstance(250, 240,  java.awt.Image.SCALE_SMOOTH));
		imagenNombre = new ImageIcon(new ImageIcon(getClass().getResource("Pid.png")).getImage().getScaledInstance(350, 40,  java.awt.Image.SCALE_SMOOTH));

	}


	public int effectiveness(Pokemon obj) {
		if(obj instanceof Fuego){
			return 20;
		}
		else if(obj instanceof Tierra){
			return 10;
		}
		else if(obj instanceof Agua){
			return 40;
		}
		else return 0;

	}
	public void dibujate(Graphics g) {
		g.drawImage(imagen.getImage(), x, y, null);
	}
	@Override
	public void animacionAtaque(Graphics g, boolean amigo) {
		if(amigo){
		 g.drawImage(imagenAtaque.getImage(), x+275, y-5, null);//cuando es enemigo la imagen aparece del lado contrario
		}
		else{
			 g.drawImage(imagenAtaque.getImage(), x-275, y+130, null);//cuando es enemigo la imagen aparece del lado contrario

		}
	}
}
